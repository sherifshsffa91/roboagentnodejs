const sortJson = require('sort-json');
const helpers = require('./files/helpers');
const pages=require('./files/page_handlers')
const prefixes=require('./files/prefixes')
var cpaas = require('@avaya/cpaas');
const fs = require('fs');
const { default: axios } = require('axios');
var ix = cpaas.inboundXml;
var enums = cpaas.enums;

module.exports = function(app, db) {
    app.get('/', (req, res) => {
        res.send("Hello World")
    });

    app.get('/db_get_all',(req, res) => {
        db.collection("robo_agent_db_c").find({}).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            const myres=[];
            for (var i of result){
                delete i._id
                const options = { ignoreCase: true, reverse: true, depth: 1};
                i=sortJson(i,options)
                myres.push(i)
            }
            const data=JSON.stringify({"result":myres})
            res.send(data);     
        })
    })

    app.get('/db_get_all_fe/:_id',(req, res) => {
        const id=req.params._id;
        console.log(id);
        db.collection("robo_agent_db_c").findOne({_id:Number(id)},(err, result)=> {
            if (err) throw err;
            console.log(result);
            const options = { ignoreCase: true, reverse: true, depth: 1};
            result=sortJson(result,options)
            const data=JSON.stringify({"result":result})
            res.send(data);
        });
    })

    app.post('/db_replace',(req, res) => {
        const id=req.body._id
        const options = {upsert: true};
        db.collection("robo_agent_db_c").replaceOne({_id:Number(id)},req.body,options,(err, result)=> {
            if (err) {
                res.send(JSON.stringify({"result":false}))
                throw err;
            }
            res.send(JSON.stringify({"result":true}))
        })
    })


    app.get('/db_delete_id/:_id',(req, res) => {
        const id= req.params._id
        console.log('here ',id);
        db.collection("robo_agent_db_c").remove({_id:Number(id)},(err, result)=> {
            console.log(result);
            if (err) {
                res.send(JSON.stringify({"result":false}))
                throw err;
            }
            res.send(JSON.stringify({"result":true}))
        })
    })


    app.get('/max_id',(req, res) => {
        const id= req.params._id
        db.collection("robo_agent_db_c").find().sort([["_id",-1]]).limit(1).toArray((err, result)=> {
            console.log(result);
            if (err) {
                res.send(JSON.stringify({"_id":0}))
                throw err;
            }
            res.send(JSON.stringify(result[0]))
        })
    })

    app.post('/db_add_keys',(req, res) => {
        var body=req.body
        const id= req.body._id

        db.collection("robo_agent_db_c").findOne({_id:Number(id)},(err, result)=> {
            if (err) throw err;
            console.log(result);
            if (!result) {
                res.send(JSON.stringify({"result":false}))
            } else{
            delete result["_id"]
            const keys=Object.keys(body)
            const db_keys=Object.keys(result)
            console.log('keys are ',keys,db_keys);
            for (const i of keys) {
                if (! db_keys.includes(i)){
                    result[i]=''
                }
                if (result[i] != body[i] ) {
                    result[i]=body[i]
                }
            }
            console.log("modified is ",result);
            const options = {upsert: true};
            db.collection("robo_agent_db_c").replaceOne({_id:Number(id)},result,options,(err,doc)=>{
                if (err) throw err;
                res.send(JSON.stringify(result))
            })
        }
        })
    })

    app.post('/db_replace_key',(req, res) => {
        const id=req.body._id
        console.log("id and body is ",id,req.body);
        const options = {upsert: true};
        var body=req.body
        delete body._id
        var key=Object.keys(body)[0]
        console.log("key is ",key);
        db.collection("robo_agent_db_c").findOne({_id:Number(id)},(err, result)=> {
            if (err) {
                res.send(JSON.stringify({"result":false}))
                throw err;
            }
            result[key]=body[key]
            db.collection("robo_agent_db_c").replaceOne({_id:Number(id)},result,options,(err, result)=> {
                console.log(result);
                if (err) {
                    res.send(JSON.stringify({"result":false}))
                    throw err;
                }
                res.send(JSON.stringify({"result":true}))
            })
        })
    })



    app.get('/make_call/:number',async(req,res)=>{
        const number=req.params.number;
        const gateway='https://api.zang.io'
        let url=gateway+'/v2/Accounts/AC777c3e329afbabdd1ba141e69df65e53/Calls.json'
        const params={
            auth:{
                username:prefixes.account_sid,
                password:prefixes.authtoken
            },
            params:{
                "From": "+15102281128",
                "To": number.toString(),
                "Url": prefixes.backend_URL+"/callback_url",
                "Record": true
        }}
        let caller;
        try {
            caller=await helpers.posthttp(url,{},params)
            let call_sid=caller["data"]["sid"]
            url=prefixes.backend_URL+"/call_recordings/"+call_sid.toString()
        } catch (error) {
            res.send(JSON.stringify({"result":false}))
        }

        console.log("status is",caller["status"]);
        if (!caller ||caller["status"] !=200) {
            res.send(JSON.stringify({"result":false}))
        }else{
            res.send(JSON.stringify({
                "result":true,
                "recording_url":url,
                "description":"visit this link after you end the call to get call details"                
            }));
        }        
    })

    app.post('/callback_url',async (req,res)=>{
        console.log("callback url here");
        const avaya_n = '+15102281128';
        let xmlDefinition = ix.response({content: [
            ix.redirect({
                url:prefixes.voice_call_xml
            })
        ]});

        const resXML=await ix.build(xmlDefinition).then(function(resXML){
            return resXML
        }).catch(function(err){
            console.log('The generated XML is not valid!', err);
        });
        res.type('application/xml');
        res.send(resXML)
    })

    app.get('/call_recordings/:call_sid',async (req, res) => {
        let call_sid=req.params.call_sid
        let url='https://api.zang.io/v2/Accounts/'+prefixes.account_sid+'/Recordings.json'
        const auth={
            auth:{
                username:prefixes.account_sid,
                password:prefixes.authtoken
            }
        }
        let recordings_list= await axios.get(url,auth).then(res=>{
            return res["data"]["recordings"]
        })
        let call_time='';
        let result='',recording_url,source_filename;

        for (const i of recordings_list) {
            if (i["call_sid"] == call_sid) {
                recording_url=i["recording_url"]
                call_time=i["date_created"]
                if (recording_url != '') {
                    console.log(recording_url);
                    let filename = recording_url.split('/')[recording_url.split('/').length - 1]
                    let dir ='/tmp/'
                    source_filename = dir + filename
                    let myfile=fs.createWriteStream(source_filename)
                    let doc =await axios.get(recording_url,{responseType: "stream"}).then(async(res)=>{
                        res.data.pipe(myfile)
                        await new Promise(fulfill => myfile.on("finish", fulfill));
                        // myfile.on('finish',async()=>{
                        result=await helpers.uploadFile(source_filename,filename).catch(console.error);
                        console.log("uploaded succesfully and result is",result);
                        // })
                    })
                }
            }
        }
        console.log("result is ",result);
        if (result != '') {
            let response=result
            let gcs_uri=response["gcs_uri"]
            let y=await helpers.transcribe_gcs_with_word_time_offsets(source_filename)
            console.log("timestamps is ",y);
            fs.unlinkSync(source_filename)
            let conversation_id=y[0]
            let timestamps=y[1]
            url= prefixes.backend_URL+'/db_add_keys'
            let body={
                "_id":conversation_id,
                "public_url":response["public_url"],
                "question_timestamps":timestamps
            }
            // start from here 
            let r=await axios.post(url,body).then(res=>{
                return res.data
            })

            response["timestamps"]=timestamps
            response['conversation_id']=conversation_id
            response["call_start_time"]=call_time
            res.send(JSON.stringify(response))
        }
        else{
            res.send(JSON.stringify({"result":"no recordings found"}))
        }

    })


    app.post('/webhook',async (req, res) => {
        let main_request=req.body
        let tag=main_request["fulfillmentInfo"]["tag"]

        if (tag ==="conversation-id") {
            res.send(await pages.page_conversation_id(main_request))
        }

        else if (tag ==="zip-code") {
            res.send(await pages.page_zip_code(main_request))
        }

        else if (tag ==="zip-code-no-match") {
            res.send(await pages.page_zip_code_no_match(main_request))
        }

        else if (tag ==="name-first") {
            res.send(await pages.page_name_first(main_request))
        }
        
        else if (tag ==="name-first-no-match") {
            res.send(await pages.page_name_first_no_match(main_request))
        }
        
        else if (tag ==="name-last") {
            res.send(await pages.page_name_last(main_request))
        }
        
        else if (tag ==="name-last-no-match") {
            res.send(await pages.page_name_last_no_match(main_request))
        }

        else if (tag ==="name-middle") {
            res.send(await pages.page_name_middle(main_request))
        }

        else if (tag ==="name-middle-no-match") {
            res.send(await pages.page_name_middle_no_match(main_request))
        }

        else if (tag ==="born-date") {
            res.send(await pages.page_born_date(main_request))
        }

        else if (tag ==="born-date-no-match") {
            res.send(await pages.page_born_date_no_match(main_request))
        }

        else if (tag ==="street-address") {
            res.send(await pages.page_street_address(main_request))
        }

        else if (tag ==="street-address-no-match") {
            res.send(await pages.page_street_address_no_match(main_request))
        }

        else if (tag ==="state") {
            res.send(await pages.page_state(main_request))
        }

        else if (tag ==="state-no-match") {
            res.send(await pages.page_state_no_match(main_request))
        }

        else if (tag ==="city") {
            res.send(await pages.page_city(main_request))
        }

        else if (tag ==="city-no-match") {
            res.send(await pages.page_city_no_match(main_request))
        }
        })

};










