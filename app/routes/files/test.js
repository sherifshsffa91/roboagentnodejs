const { default: axios } = require("axios")
const {Storage} = require('@google-cloud/storage');
const storage = new Storage();
const speech = require('@google-cloud/speech');
const client = new speech.SpeechClient();
const {exec}= require('child_process')
const sound = require("sound-play");

const {join, execAsync}=require ('async-child-process')
// process.env.GOOGLE_APPLICATION_CREDENTIALS="E:\\freelancer_new\\robo-agent\\nodejs_be\\app\\routes\\files\\robo-agent-1b3b40dd16f2.json";




// console.log(process.env);
// const config = {
//     enableWordTimeOffsets: true,
//     enableAutomaticPunctuation: true,
//     encoding: encoding,
//     sampleRateHertz: sampleRateHertz,
//     languageCode: languageCode,
//   };

//   const audio = {
//     uri: "gs://avaya-recordings/RE777c3e3260a0d270f90d438a8e518d88.mp3",
//   };
  
//   const request = {
//     config: config,
//     audio: audio,
//   };

//   response.results.forEach(result => {
//     console.log(`Transcription: ${result.alternatives[0].transcript}`);
//     result.alternatives[0].words.forEach(wordInfo => {
//       // NOTE: If you have a time offset exceeding 2^32 seconds, use the
//       // wordInfo.{x}Time.seconds.high to calculate seconds.
//       const startSecs =`${wordInfo.startTime.seconds}`
//       const endSecs =`${wordInfo.endTime.seconds}`
//       console.log(`Word: ${wordInfo.word}`);
//       console.log(`\t ${startSecs} secs - ${endSecs} secs`);
//     });
//   });

async function transcribe_gcs_with_word_time_offsets(filename) {
    const config = {
        enableWordTimeOffsets: true,
        enableAutomaticPunctuation: true,
        encoding: "ENCODING_UNSPECIFIED",
        sampleRateHertz: 16000,
        languageCode: "en-US",
      };
      const file=fs.readFileSync(filename)
      const audioBytes=file.toString('base64')
      const audio = {
        content:audioBytes,
      };

      const request = {
        config: config,
        audio: audio,
      };
      const [operation] = await client.longRunningRecognize(request);
      const [response] = await operation.promise();
      let timestamps=[];
      let conversation_id=0;
      for (let result of response.results) {
          console.log(result.alternatives[0].transcript);
          let alternative = result.alternatives[0]
          let splitted=alternative.transcript.split(' ').join('').split(',').join('').split('.').join('').split('?').join('').toLowerCase()
          if (splitted.search('conversationid') != -1 ) {
            var regex = /\d+/g;
            var x = alternative.transcript.match(regex)
            if (x) {
              conversation_id=Number(x[0])
            }
          }
          for (let word_info of alternative.words) {
            if (word_info.word.includes('?') != false) {
                let word= word_info.word
                // let start_time = word_info.startTime
                const startSecs =`${word_info.startTime.seconds}` +
                '.' + 
                word_info.startTime.nanos / 100000000;
                console.log(word,startSecs);     
                timestamps.push(startSecs)
            }
          }
      }
      return [conversation_id,timestamps]
    }



const fs = require('fs');


async function uploadFile(filePath,destFileName) {
    const bucketName ="avaya-recordings"
    return storage.bucket(bucketName).upload(filePath, {
      destination: destFileName,
});
}




(async ()=>{
    let recording_url="http://recordings.telapi.com/RB66944b6de9b4462e9efbf3675ddabf25/RE777c3e3260a0d270f90d438a8e518d88.mp3"
    // let filename = recording_url.split('/')[recording_url.split('/').length - 1].split('.')[0];
    let filename = recording_url.split('/')[recording_url.split('/').length - 1];
    let source_file_name='./tmp/'+filename
    let myfile=fs.createWriteStream(source_file_name)
    let x=await axios.get(recording_url,{responseType: "stream"}).then((res)=>{
        res.data.pipe(myfile)
        myfile.on('finish',async()=>{
          console.log('uploading');
          let result=await uploadFile(source_file_name,filename).catch(console.error);
          let y=await transcribe_gcs_with_word_time_offsets(source_file_name)
          fs.unlinkSync(source_file_name)
          console.log(y);      
        })
    })



})();

console.log('try');



// @app.route('/call_recordings/<call_sid>', methods=['GET','POST'])
// def get_call_recording(call_sid):
//     url='https://api.zang.io/v2/Accounts/'+account_sid+'/Recordings.json'
//     auth=HTTPBasicAuth(username=account_sid,password=authtoken)
//     recordings_list=requests.get(url=url,auth=auth)
//     recordings_list=recordings_list.json()["recordings"]
//     call_time=''
//     result=''
//     for i in recordings_list:
//         if i["call_sid"] == call_sid:
//             recording_url=i["recording_url"]
//             call_time=i["date_created"]
//             if recording_url != '':
//                 print(recording_url)
//                 filename = recording_url.split('/')[len(recording_url.split('/')) - 1].split('.')[0]
//                 dir = '/tmp/'
//                 source_filename = dir + filename + '.mp3'
//                 doc = requests.get(recording_url)
//                 print('files stored in /tmp/ folder is ',os.listdir(dir))
//                 with open(source_filename, 'wb') as f:
//                     f.write(doc.content)

//                 result = upload_blob(source_file_name=source_filename, destination_blob_name=filename)
//                 print('files stored in /tmp/ folder is ',os.listdir(dir))
//                 for f in os.listdir(dir):
//                     if str(f) == filename+'.mp3':
//                         os.remove(os.path.join(dir, f))
//                 print('files stored in /tmp/ folder is ',os.listdir(dir))
//             break

//     if result != '':
//         response = result
//         gcs_uri = response["gcs_uri"]
//         conversation_id,timestamps=transcribe_gcs_with_word_time_offsets(gcs_uri)
//         # if len(timestamps) >= 1:
//         url=backend_URL+'/db_add_keys'
//         body={}
//         body["_id"]=int(conversation_id)
//         body["public_url"]=response["public_url"]
//         # body["gcs_uri"]=response["gcs_uri"]
//         body["question_timestamps"]=timestamps
//         r=requests.post(url=url,json=body)
//         response["timestamps"]=timestamps
//         response['conversation_id']=conversation_id
//         response["call_start_time"]=call_time
//         return jsonify(response)
//     else:
//         return jsonify({"result":"no recordings found"})


// def upload_blob(source_file_name, destination_blob_name,bucket_name='avaya-recordings'):
//     from google.cloud import storage
//     os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "robo-agent-1b3b40dd16f2.json"
//     storage_client = storage.Client()
//     bucket = storage_client.bucket(bucket_name)
//     blob = bucket.blob(destination_blob_name)
//     blob.upload_from_filename(source_file_name)
//     result="File {} uploaded to {}".format(
//             source_file_name, destination_blob_name
//         )
//     gcs_uri="gs://"+bucket_name+"/"+destination_blob_name
//     public_url="https://storage.cloud.google.com/"+bucket_name+"/"+destination_blob_name
//     gcs = {}
//     gcs["result"] = result
//     gcs["gcs_uri"] = gcs_uri
//     gcs["public_url"] = public_url
//     return gcs



// def transcribe_gcs_with_word_time_offsets(gcs_uri):
//     from google.cloud import speech
//     import re
//     os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "robo-agent-1b3b40dd16f2.json"
//     client = speech.SpeechClient()
//     audio = speech.RecognitionAudio(uri=gcs_uri)
//     # encoding = enums.RecognitionConfig.AudioEncoding.ENCODING_UNSPECIFIED
//     config = speech.RecognitionConfig(
//         encoding=speech.RecognitionConfig.AudioEncoding.ENCODING_UNSPECIFIED,
//         sample_rate_hertz=16000,
//         language_code="en-US",
//         enable_word_time_offsets=True,
//         enable_automatic_punctuation=True
//     )

//     operation = client.long_running_recognize(config=config, audio=audio)

//     print("Waiting for operation to complete...")
//     result = operation.result(timeout=90)
//     timestamps = []
//     conversation_id=0
//     for result in result.results:
//         alternative = result.alternatives[0]

//         if alternative.transcript.replace(' ','').replace(',','').replace('.','').replace('?','').lower().find('conversationid') != -1:
//             x = re.findall('[0-9]+', alternative.transcript)
//             if x:
//                 conversation_id=int(x[0])
//         for word_info in alternative.words:
//             if word_info.word.find('?') != -1 :
//                 word = word_info.word
//                 start_time = word_info.start_time
//                 timestamps.append(start_time.total_seconds())
//                 end_time = word_info.end_time
//     return conversation_id, timestamps