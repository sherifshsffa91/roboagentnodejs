const helpers=require('./helpers')
const responses= require('./responses')
const prefixes=require('./prefixes')
const {isValidStateInput,retrieveStateInformation} = require("usa-state-validator")


async function page_conversation_id(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let res=await helpers.get_max_id()
    let id= Number(res)+1
    let cx_text='Hello. Your conversation ID is '+id.toString()+'.'
    let save=await helpers.mongo_kv(main_request,"_id",id);
    return responses.message(cx_text,'',save)
}

async function page_zip_code(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let zip_code = parameters["numbers"][0]

    if (zip_code.length == 5) {
        return responses.next(await helpers.mongo_kv(main_request,'01-z'+prefixes.notmodified+prefixes.right,zip_code))
    } else {
        return responses.message("Not valid USA zip code",currentPage,await helpers.mongo_kv(main_request,'01-z'+prefixes.notmodified+prefixes.notvalid,zip_code))
    }
}

async function page_zip_code_no_match(main_request) {
    // main_request=JSON.parse(main_request)
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'01-z'+prefixes.notmodified + prefixes.nomatch , text))

}

async function page_name_first(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let name_first = parameters["name"][0]["name"]
    return responses.next(await helpers.mongo_kv(main_request,'02-fn'+prefixes.notmodified+prefixes.right,name_first))
}

async function page_name_first_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'02-fn'+prefixes.notmodified + prefixes.nomatch , text))
}

async function page_name_last(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let name_last = parameters["name"][0]["name"]
    return responses.next(await helpers.mongo_kv(main_request,'03-ln'+prefixes.notmodified+prefixes.right,name_last))
}

async function page_name_last_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'03-ln'+prefixes.notmodified + prefixes.nomatch , text))
}

async function page_name_middle(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let name_middle = parameters["letter"][0]
    return responses.next(await helpers.mongo_kv(main_request,'04-mn'+prefixes.notmodified+prefixes.right,name_middle))
}


async function page_name_middle_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'04-mn'+prefixes.notmodified + prefixes.nomatch , text))   
}


async function page_born_date(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let born_date = parameters["date"][0]
    let year =Number(born_date["year"])
    let month =Number(born_date["month"])
    let day =Number(born_date["day"])
    let age=helpers.getAge(year,month,day)
    let born_date_formatted=new Date(year,month-1,day)
    if (Number(age) >= 16) {
        return responses.next(await helpers.mongo_kv(main_request,'05-bd'+prefixes.notmodified+prefixes.right,born_date_formatted))
    } else {
        return responses.message("you must be equal to or more than 16 years old"
        ,currentPage,await helpers.mongo_kv(main_request,'05-bd'+prefixes.notmodified + prefixes.notvalid , born_date_formatted))   

    }
}


async function page_born_date_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'05-bd'+prefixes.notmodified + prefixes.nomatch , text))   

}

async function page_street_address(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let street_address = parameters["street-address"][0]
    return responses.next(await helpers.mongo_kv(main_request,'06-sa'+prefixes.notmodified+prefixes.right,street_address))
}
async function page_street_address_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'06-sa'+prefixes.notmodified + prefixes.nomatch , text))   
}
async function page_state(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let state = parameters["state"][0]
    if (isValidStateInput(state)) {
        return responses.next(await helpers.mongo_kv(main_request,'07-st'+prefixes.notmodified+prefixes.right,state))
    } else {
        return responses.message("state name not true",currentPage,
        await helpers.mongo_kv(main_request,'07-st'+prefixes.notmodified + prefixes.notvalid , state))
    }

}
async function page_state_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'07-st'+prefixes.notmodified + prefixes.nomatch , text))   
}

async function page_city(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let city = parameters["city"][0]
    let state = parameters["state"][0]
    if (helpers.get_city_opendata(city,state)) {
        return responses.next(await helpers.mongo_kv(main_request,'08-c'+prefixes.notmodified+prefixes.right,city))
    } else {
        return responses.message("city not existed in State",currentPage,
        await helpers.mongo_kv(main_request,'08-c'+prefixes.notmodified + prefixes.notvalid , city))
    }
}


async function page_city_no_match(main_request) {
    let currentPage=main_request["pageInfo"]["currentPage"]
    let parameters=main_request["sessionInfo"]["parameters"]
    let text = main_request["text"]
    return responses.message("",currentPage,await helpers.mongo_kv(main_request,'08-c'+prefixes.notmodified + prefixes.nomatch , text))
}


// async function page_state_no_match(main_request) {
    
// }





module.exports={
    page_conversation_id,page_zip_code,page_zip_code_no_match,
    page_name_first,page_name_first_no_match,page_name_last,page_name_last_no_match,
    page_city_no_match,page_city,page_state_no_match,page_state,page_street_address_no_match,
    page_street_address,page_born_date_no_match,page_born_date,page_name_middle_no_match,
    page_name_middle
}




