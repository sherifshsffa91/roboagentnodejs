function next(mongodb_saved) {
    return message('','', mongodb_saved=mongodb_saved)
}

function message(text='', page='' ,mongodb_saved={}) {
    let fulfillmentResponse = {
        "fulfillmentResponse": {
            "messages": [{
                "text": {
                    "text": [
                        text
                    ]
                },
            }
            ]
        },
        "target_page": page
    }

    const sessioninfo= {
        "parameters": {"mongodb_saved":mongodb_saved}
    }
    fulfillmentResponse.sessionInfo = sessioninfo;
    return JSON.stringify(fulfillmentResponse)
}

module.exports={message,next}