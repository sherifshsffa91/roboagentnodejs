const responses = require('./responses');
const request= require('sync-request')
const axios = require('axios')
const prefixes= require('./prefixes')
const {Storage} = require('@google-cloud/storage');
const speech = require('@google-cloud/speech');
const client = new speech.SpeechClient();
const storage = new Storage();
const fs=require('fs')

// use coming function similar to
// let st= test.get_city_opendata("columbus","ohio")

function get_city_opendata(city,state) {
    try {
        const api="https://countriesnow.space/api/v0.1/countries/state/cities";
        let res=  request("POST",api,{json:{
            "country":"United States",
            "state":state
        }})
        res=JSON.parse(res.getBody('utf8'));
        const cities= res["data"]
        for (const i in cities) {
            if (cities[i].toLowerCase() == city.toLowerCase()) {
                return true
            }
        }
        return false
    } catch (error) {
        return false
    }
}

async function mongo_kv(main_request,key,value) {
    let mongodb_saved;
    try {
        mongodb_saved=main_request["sessionInfo"]["parameters"]["mongodb_saved"]
        let mongodb_saved_be=await gethttp(prefixes.backend_URL + '/db_get_all_fe/'+Number(mongodb_saved["_id"]).toString())
        mongodb_saved=mongodb_saved_be["result"]
    } catch (error) {
        console.log("error is ",error);
        mongodb_saved={}
    }

    let val_list;
    if (mongodb_saved[key] != undefined) {
        val_list=mongodb_saved[key]
    } else {
        val_list=[]
    }
    // console.log("before ",mongodb_saved);
    if (key != "_id") {
        val_list.push(value)
        mongodb_saved[key]=val_list
    } else{
        mongodb_saved["_id"]=value
    }

    // console.log("after ",mongodb_saved);
    let save= await posthttp(prefixes.backend_URL+'/db_replace',mongodb_saved)
    return mongodb_saved
}

function get_max_id() {
    return axios.get(prefixes.backend_URL + '/max_id').then(res=>{
        let id_request =res["data"]
        id_request=id_request["_id"]
        return id_request
    })
}

function gethttp (url){
        return axios.get(url).then(res=>{
            return res["data"]
        })
}

function posthttp(url,body,params,auth={}){
    return axios.post(url,body,params).then(res=>{
        return res
    }).catch(error=>{
        return false
    })
}

function getAge(year,month,day) 
{
    var today = new Date();
    var birthDate = new Date(year,month-1,day);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--;
    }
    return age;
}


async function uploadFile(filePath,destFileName) {
    // process.env.GOOGLE_APPLICATION_CREDENTIALS="robo-agent-1b3b40dd16f2.json"
    const bucketName ="avaya-recordings"
    await storage.bucket(bucketName).upload(filePath, {
      destination: destFileName,
    });
    let result=`File ${filePath} uploaded to ${destFileName}`

    let gcs_uri="gs://"+bucketName+"/"+destFileName
    let public_url="https://storage.cloud.google.com/"+bucketName+"/"+destFileName
    let gcs = {
        "result":result,
        "gcs_uri":gcs_uri,
        "public_url":public_url
    }
    return gcs
}

async function transcribe_gcs_with_word_time_offsets(filename) {
    const config = {
        enableWordTimeOffsets: true,
        enableAutomaticPunctuation: true,
        encoding: "ENCODING_UNSPECIFIED",
        sampleRateHertz: 16000,
        languageCode: "en-US",
      };
      const file=fs.readFileSync(filename)
      const audioBytes=file.toString('base64')
      const audio = {
        content:audioBytes,
      };

      const request = {
        config: config,
        audio: audio,
      };
      const [operation] = await client.longRunningRecognize(request);
      const [response] = await operation.promise();
      let timestamps=[];
      let conversation_id=0;
      for (let result of response.results) {
          console.log(result.alternatives[0].transcript);
          let alternative = result.alternatives[0]
          let splitted=alternative.transcript.split(' ').join('').split(',').join('').split('.').join('').split('?').join('').toLowerCase()
          if (splitted.search('conversationid') != -1 ) {
            var regex = /\d+/g;
            var x = alternative.transcript.match(regex)
            if (x) {
              conversation_id=Number(x[0])
            }
          }
          for (let word_info of alternative.words) {
            if (word_info.word.includes('?') != false) {
                let word= word_info.word
                // let start_time = word_info.startTime
                const startSecs =`${word_info.startTime.seconds}` +
                '.' + 
                word_info.startTime.nanos / 100000000;
                console.log(word,startSecs);     
                timestamps.push(startSecs)
            }
          }
      }
      return [conversation_id,timestamps]
    }

module.exports={get_city_opendata,mongo_kv,get_max_id,getAge,posthttp,gethttp,
    uploadFile,transcribe_gcs_with_word_time_offsets}